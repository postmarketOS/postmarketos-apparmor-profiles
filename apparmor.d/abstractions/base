# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2002-2009 Novell/SUSE
#    Copyright (C) 2009-2011 Canonical Ltd.
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

# FIXME: this file contains several comments referencing glibc, which is not
# used in Alpine. Investigate whether they are relevant, and adjust or remove.

  abi <abi/3.0>,

  include <abstractions/crypto>

  # (Note that the ldd profile has inlined this file; if you make
  # modifications here, please consider including them in the ldd
  # profile as well.)

  # The __canary_death_handler function writes a time-stamped log
  # message to /dev/log for logging by syslogd. So, /dev/log, timezones,
  # and localisations of date should be available EVERYWHERE, so
  # StackGuard, FormatGuard, etc., alerts can be properly logged.
  /dev/log                       w,
  /dev/random                    r,
  /dev/urandom                   r,
  # Allow access to the uuidd daemon (this daemon is a thin wrapper around
  # time and getrandom()/{,u}random and, when available, runs under an
  # unprivilged, dedicated user).
  @{run}/uuidd/request           r,
  /etc/locale/**          r,
  /etc/locale.alias       r,
  /etc/localtime          r,
  /usr/share/locale-bundle/**    r,
  /usr/share/locale-langpack/**  r,
  /usr/share/locale/**           r,
  /usr/share/**/locale/**        r,
  /usr/share/zoneinfo/           r,
  /usr/share/zoneinfo/**         r,
  /usr/share/X11/locale/**       r,

  # used by glibc when binding to ephemeral ports
  /etc/bindresvport.blacklist    r,

  /lib/ld-musl-*.so                   mr,

  # we might as well allow everything to use common libraries
  /{usr/,}lib/**                      r,
  /{usr/,}lib/**.so*                  mr,

  # /dev/null is pretty harmless and frequently used
  /dev/null                      rw,
  # as is /dev/zero
  /dev/zero                      rw,
  # recent glibc uses /dev/full in preference to /dev/null for programs
  # that don't have open fds at exec()
  /dev/full                      rw,

  # Sometimes used to determine kernel/user interfaces to use
  @{PROC}/sys/kernel/version     r,
  # Depending on which glibc routine uses this file, base may not be the
  # best place -- but many profiles require it, and it is quite harmless.
  @{PROC}/sys/kernel/ngroups_max r,

  # glibc's sysconf(3) routine to determine free memory, etc
  @{PROC}/meminfo                r,
  @{PROC}/stat                   r,
  @{PROC}/cpuinfo                r,
  /sys/devices/system/cpu/       r,
  /sys/devices/system/cpu/online r,

  # some applications will display license information
  /usr/share/common-licenses/**  r,

  # glibc statvfs
  @{PROC}/filesystems            r,

  # glibc malloc (man 5 proc)
  @{PROC}/sys/vm/overcommit_memory r,

  # Allow determining the highest valid capability of the running kernel
  @{PROC}/sys/kernel/cap_last_cap r,

  # Allow other processes to read our /proc entries, futexes, perf tracing and
  # kcmp for now (they will need 'read' in the first place). Administrators can
  # override with:
  #   deny ptrace (readby) ...
  ptrace (readby),

  # Allow other processes to trace us by default (they will need 'trace' in
  # the first place). Administrators can override with:
  #   deny ptrace (tracedby) ...
  ptrace (tracedby),

  # Allow us to ptrace read ourselves
  ptrace (read) peer=@{profile_name},

  # Allow unconfined processes to send us signals by default
  signal (receive) peer=unconfined,

  # Allow us to signal ourselves
  signal peer=@{profile_name},

  # Checking for PID existence is quite common so add it by default for now
  signal (receive, send) set=("exists"),

  # Allow us to create and use abstract and anonymous sockets
  unix peer=(label=@{profile_name}),

  # Allow unconfined processes to us via unix sockets
  unix (receive) peer=(label=unconfined),

  # Allow us to create abstract and anonymous sockets
  unix (create),

  # Allow us to getattr, getopt, setop and shutdown on unix sockets
  unix (getattr, getopt, setopt, shutdown),

  # Include additions to the abstraction
  include if exists <abstractions/base.d>
