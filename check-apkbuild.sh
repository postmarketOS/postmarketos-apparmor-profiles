#!/bin/sh -e
# SPDX-License-Identifier: GPL-2.0-only
# Copyright 2021 Oliver Smith

# This script gets called from check() in the APKBUILD. Verify that _profiles
# has all profiles from this git repository, and nothing extra. Only then it
# will correctly generate one subpackage for each profile.

if [ -z "$1" ]; then
	echo "usage: check-apkbuild.sh PROFILES_STR"
	exit 1
fi

PROFILES="$(echo "$1" | tr -s '\n\t' ' ')"
error=0

# Verify that each profile from the argument exists
for profile in $PROFILES; do
	if ! [ -f apparmor.d/"$profile" ]; then
		echo "ERROR: profile '$profile' not found, remove from APKBUILD"
		error=1
	fi
done

# Verify that the git repo doesn't have additional profiles
for i in apparmor.d/*; do
	# Only care about files
	if ! [ -f "$i" ]; then
		continue
	fi

	profile="$(basename "$i")"
	case " $PROFILES " in
		*" $profile "*)
			continue
			;;
	esac

	echo "ERROR: profile '$profile' missing from APKBUILD"
	error=1
done

exit $error
