# SPDX-License-Identifier: GPL-2.0-only
# Copyright 2002-2009 Novell/SUSE
# Copyright 2010-2016 Canonical Ltd.
# Copyright 2021 Oliver Smith

all: local

DESTDIR=/
PROFILES_DEST=${DESTDIR}/etc/apparmor.d
PROFILES_SOURCE=./apparmor.d
ABSTRACTIONS_SOURCE=./apparmor.d/abstractions

SUBDIRS=$(shell find ${PROFILES_SOURCE} -type d -print)
TOPLEVEL_PROFILES=$(filter-out ${SUBDIRS}, $(wildcard ${PROFILES_SOURCE}/*))

PYTHONPATH=
PARSER?=/sbin/apparmor_parser
LOGPROF?=aa-logprof

PWD=$(shell pwd)

ifndef VERBOSE
  Q=@
else
  Q=
endif

local:
	@echo "*** Creating local profiles"
	$(Q)for profile in ${TOPLEVEL_PROFILES}; do \
		fn=$$(basename $$profile); \
		echo "# Site-specific additions and overrides for '$$fn'" \
			> ${PROFILES_SOURCE}/local/$$fn; \
		grep "include[[:space:]]\\+if[[:space:]]\\+exists[[:space:]]\\+<local/$$fn>" \
			"$$profile" >/dev/null \
			|| { echo "$$profile doesn't contain include if exists <local/$$fn>" ; \
			     exit 1; } ; \
	done

.PHONY: install
install: local
	$(Q)install -m 755 -d ${PROFILES_DEST}
	$(Q)install -m 755 -d ${PROFILES_DEST}/disable
	$(Q)for dir in ${SUBDIRS} ; do \
	    	install -v -m 755 -d "${PROFILES_DEST}/$${dir#${PROFILES_SOURCE}}" ; \
	done
	$(Q)for file in $$(find ${PROFILES_SOURCE} -type f -print) ; do \
	    	install -v -m 644 "$${file}" \
			"${PROFILES_DEST}/$$(dirname $${file#${PROFILES_SOURCE}})" ; \
	done

LOCAL_ADDITIONS=$(filter-out ${PROFILES_SOURCE}/local/README, $(wildcard ${PROFILES_SOURCE}/local/*))
.PHONY: clean
clean:
	-rm -f ${LOCAL_ADDITIONS} .parser.conf

IGNORE_FILES=
CHECK_PROFILES=$(filter-out ${IGNORE_FILES} ${SUBDIRS}, $(wildcard ${PROFILES_SOURCE}/*))
# use find because Make wildcard is not recursive:
CHECK_ABSTRACTIONS=$(shell find ${ABSTRACTIONS_SOURCE} -type f -print)

.PHONY: check
check: check-parser check-logprof check-abstractions.d

.PHONY: check-parser
check-parser: local
	@echo "*** Checking profiles from ${PROFILES_SOURCE} against apparmor_parser"
	$(Q)touch .parser.conf
	$(Q)for profile in ${CHECK_PROFILES} ; do \
	        [ -n "${VERBOSE}" ] && echo "Testing $${profile}" ; \
		${PARSER} \
			--config-file=.parser.conf \
			-S \
			-b ${PWD}/apparmor.d $${profile} \
			> /dev/null || exit 1; \
	done

	@echo "*** Checking abstractions from ${ABSTRACTIONS_SOURCE} against apparmor_parser"
	$(Q)for abstraction in ${CHECK_ABSTRACTIONS} ; do \
	        [ -n "${VERBOSE}" ] && echo "Testing $${abstraction}" ; \
		echo "abi <abi/3.0>, #include <tunables/global> profile test { #include <$${abstraction}> }" \
			| ${PARSER} \
				--config-file=.parser.conf \
				-S \
				-b ${PWD}/apparmor.d \
				-I ${PWD} > /dev/null \
			|| exit 1; \
	done

.PHONY: check-logprof
check-logprof: local
	@echo "*** Checking profiles from ${PROFILES_SOURCE} against logprof"
	$(Q)${LOGPROF} -d ${PROFILES_SOURCE} -f /dev/null || exit 1

.PHONY: check-abstractions.d
check-abstractions.d:
	@echo "*** Checking if all abstractions (with a few exceptions) contain include if exists <abstractions/*.d>"
	$(Q)cd apparmor.d/abstractions && for file in * ; do \
		test -d "$$file" &&  continue ;  \
		grep -q "^  include if exists <abstractions/$${file}.d>$$" $$file \
			|| { echo "$$file does not contain 'include if exists <abstractions/$${file}.d>'"; \
			     exit 1; } ; \
	done
